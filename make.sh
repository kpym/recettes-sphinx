#!/bin/sh

cd "$(dirname "$0")"

# Command file for Sphinx documentation

if [ -z "$SPHINXBUILD" ]; then
    SPHINXBUILD=sphinx-build
fi
SOURCEDIR=source
BUILDDIR=build
SPHINXPROJ=Recettes

# if $SPHINXBUILD is not in the path
if ! command -v "$SPHINXBUILD" > /dev/null; then
  echo "sphinx-build not found"
  exit 1
fi

# If no arguments were given, show the help message
if [ -z "$1" ]; then
    $SPHINXBUILD -M help %SOURCEDIR% %BUILDDIR% %SPHINXOPTS%
    exit 1
fi

# run $SPHINXBUILD with the given arguments
$SPHINXBUILD -M $1 $SOURCEDIR $BUILDDIR $SPHINXOPTS