# Makrouts

## Ingrédients

### Pour la pâte

- semoule : 500 g
- beurre : 120 g
- eau : 230 ml
- eau de fleur d'oranger : 45 g (≈ 3 cs)

### Pour la farce
- 100 g de dattes
- eau de fleur d'oranger : 30–45 g (≈ 2–3 cs, à ajuster)

### Pour le sirop
- miel : 150 g
- eau de fleur d'oranger : 50 ml

## Temps

- préparation : 1 h
- cuissons : 20 min

## Préparation

- Mélanger les quatre premiers ingrédients jusqu'à obtenir une pâte homogène. Former deux boudins de 4–5 cm de diamètre, puis les placer au réfrigérateur pour refroidir.
- Mixer les dattes avec un peu d'eau de fleur d'oranger jusqu'à obtenir une texture souple et homogène.
- Garnir les boudins avec la préparation de dattes, refermer soigneusement, puis couper en tranches.

## Cuisson

- Faire revenir les tranches dans de l'huile d'arachide sur feu moyen-fort, quelques minutes de chaque côté, jusqu'à ce qu'elles soient dorées.
- Égoutter sur du papier absorbant pour retirer l'excédent d'huile.

## Finition

Une fois les tranches cuites, les recouvrir du mélange de miel et d'eau de fleur d'oranger. Utiliser un pinceau ou tremper les tranches directement dans le liquide.
