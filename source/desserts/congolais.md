# Congolais

## Ingrédients (pour 16)
- noix de coco : 125 g
- sucre : 125 g
- blancs d'œuf : 2
- beurre : 7 g (une cuillère à café)

## Temps
- préparation : 10 min
- cuisson : 14 min

## Préparation
- Faire fondre le beurre (15–20 sec au micro-ondes).
- Rajouter les blancs au beurre fondu. Les battre avec une fourchette juste pour les mousser un peu.
- Rajouter le sucre et la noix de coco. Bien mélanger.
- Sur une plaque faire des petites pyramides avec les doigts.

## Cuisson

À 170 °C : 14 min

## Note
- Source principale : [Marmitons](https://www.marmiton.org/recettes/recette_congolais-ultra-simple_36635.aspx)
