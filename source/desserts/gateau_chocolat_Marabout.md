# Gâteau au chocolat fondant « Marabout »

## Ingrédients

- chocolat noir (Poulain 47 %) : 200 g
- beurre : 200 g
- œufs : 5
- sucre : 250 g
- farine : 1½ cuillères
- des noix : éventuellement
- vanille : éventuellement

## Temps
- préparation : 14 min
- cuisson : 21 min

## Préparation

On fait fondre au micro-ondes le chocolat (1 min 30) et le beurre (+1 min). On mélange tout (+ sucre + œufs + farine + autres) et on fait cuire.

## Cuisson

À 190 °C : 21 min

## Remarque

Tout est dans le choix du chocolat, la cuisson et le peu de farine.

