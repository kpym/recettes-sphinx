# Flan de noix de coco

## Ingrédients

- lait concentré : 1 boîte (397 g)
- œufs : 3
- lait de coco : 120 ml
- lait : 120 ml
- noix de coco râpée : 100-150 g

## Temps
- préparation : 5 min
- cuisson : 25 min

## Préparation

On mélange le tout (avec le mixeur).

## Cuisson

À 180 °C : 25 min

## Remarque

On peut tapisser le fond du plat des mangues et/ou des morceaux d'ananas.

## Sources

- [La source principale](https://github.com/charonuguid/recipes/blob/master/markdown/01_Baked_Coconut.md)
- [La vidéo](https://www.tastemade.com/videos/baked-coconut)
