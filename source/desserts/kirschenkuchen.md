# Kirschenkuchen « Babi »

## Ingrédients

- beurre : 100 g
- sucre : 225 g
- farine : 250 g
- œufs : 3
- lait : 150 ml
- levure chimique : 5 g (1/2 sachet)
- vanille
- griottes : 500 g

## Temps

- préparation : 20 min
- cuisson : 40 min

## Préparation

Faire fondre le beurre (100 g). Mélanger le beurre, le sucre (225 g), les 3 jaunes d'œufs, la farine (250 g), la levure (5 g), le lait (150 ml) et la vanille. Battre les 3 blancs en neige et les incorporer au reste. Verser dans un moule beurré. Mettre par-dessus les griottes.

## Cuisson

À 180 °C : 40 min

## Notes

- On peut remplacer les griottes par des abricots pour faire « Aprikosenkuchen ».
- Dans la recette originale le temps de cuisson est plus court (30 min) et on bat d'abord les jaunes avec le sucre.
- Si les fruits sont congelés, il faut bien les décongeler et égoutter avant.

