# Clafoutis (aux fruits)

## Ingrédients

- fruits (cerises, abricots, ...) : 500 g
- œuf : 3
- farine : 100 g
- sucre : 100 g
- lait : 250 ml
- vanille
- pincé de sel
- beurre pour le moule

## Temps
- préparation : 15 min
- cuisson : 40 min

## Préparation

Les œufs bien battus + la farine + le sucre + le lait + la vanille + le sel sont versés sur les fruits déposés dans le moule pré-beurré.

## Cuisson

À 165 °C : 40 min

