# Cookies d'Eva

## Ingrédients
_(pour 8-10 cookies)_

- beurre 1/2 sel : 85 g
- sucre roux : 125 g
- œuf : 1
- lait : 15 ml (1 c.-à-s.)
- farine : 150 g
- levure chimique : 5 g (½ sachet)
- noix de pécan salées : 100 g
- chocolat blanc : 100 g

## Temps

- préparation : 10 min
- cuisson : 10 min

## Préparation

Ramollir le beurre (20–30 sec au micro-ondes). Mélanger, dans l'ordre : le beurre, le sucre, l'œuf, le lait. Rajouter la farine et la levure, puis les mélanger au reste. Rajouter les noix et le cocolat blanc en carreaux entiers. Former 8–9 boules (de 4–5 cm de diamètre) sur une plaque, puis enfourner _(dans la partie haute)_.

## Cuisson

À 180 °C : 11–13 min _(à surveiller)_

## Note

Recette trouvée dans une BD. La recette originale est avec chocolat au lait à la place des noix et du chocolat blanc.
