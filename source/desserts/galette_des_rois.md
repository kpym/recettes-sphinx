# Galette des rois

## Ingrédients

- beurre : 100 g
- sucre : 125 g
- œufs : 2 (+1 jaune pour la finition)
- poudre d'amandes : 125 g
- pâtes feuilletées _pur beurre_ : 2
- fruits (des cerises, des pommes, framboises, abricots ...) : optionnel
- fève : 1

## Temps

- préparation : 10 min
- cuisson : 30 min

## Préparation

Pour la frangipane : on fait fondre le beurre, on rajoute le sucre, les 2 œufs et la poudre d'amande (éventuellement les fruits). On enferme la frangipane et la fève entre les deux pâtes feuilletées (on fait des rayures au couteau sur le dessus).

## Cuisson

30 min à 190 °C (après 25 minutes on badigeonne avec un jaune d'œufs et du sucre mélangés).

## Notes
- On peut mettre le troisième blanc dans la frangipane, ou faire la frangipane avec 1 œuf et 1 blanc.
- Si on utilise des fruits surgelés ce n'est pas la penne de les décongeler avant.
