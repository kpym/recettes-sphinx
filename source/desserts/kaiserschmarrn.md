# Kaiserschmarrn

## Ingrédients

### pour la pâte

- œufs : 2
- lait : 10 cl
- farine : 55 g
- sucre : 20 g
- sel

### pour la cuisson

- beurre : 10 g
- sucre : 30 g

### pour le service

- sucre glace
- compote (confiture) de pommes ou/et de prunes.

## Temps

- préparation : 15 min
- cuisson : 10 min

## Préparation

Dans un saladier, mélangez les jaunes d'œufs, la farine, le sel et le lait. Dans un second saladier, battez les blancs d'œufs en neige avec 20 g de sucre. Incorporer le blanc au reste.

Faire fondre le beur, puis verser la préparation dans la poêle. Après 4 minutes couper en 4 et retourner. Après encore 3 minutes couper en petits morceaux. Saupoudrer de 2 cuillères de sucre et continuer à dorer et caraméliser les morceaux de tout les côtés.

À servir chaud, saupoudré de sucre glace et accompagné de compote (ou confiture) de pommes ou de prunes.

## Cuisson

À poêle : 10 min

## Remarque

- On peut faire la compote de pommes comme une confiture allégée (5:1) avec vanille et cannelle.
- On peut rajouter dans la pâte des raisins secs (2 cuillères à soupe), éventuellement préalablement trempés dans de l'alcool (rhum ou cognac).
- On peut aussi rajouter de la vanille.


