# Financier

## Ingrédients
*Souvent on fait ces proportions × 3/2.*

- poudre d'amande : 50 g
- farine : 50 g
- sucre : 140 g
- beurre : 75 g
- blancs d'œuf : 4
- d'extrait de vanille : quelques gouttes
- sel : 2 pincées
- amandes effilées

## Temps
- préparation : 15 min
- cuisson : 21 min

## Préparation
- Mélanger les poudres (amande, farine, sucre, sel).
- Monter les blancs en neige. Puis les mélanger avec les poudres.
- Faire fondre le beurre (45 sec au micro-ondes). Puis l'incorporer au reste avec la vanille.
- Saupoudrer avec les amandes effilées.

## Cuisson

À 190 °C : 21 min

## Note
- Ne pas mélanger le beurre avant d'incorporer les blancs.
- Source principale : [Marmitons](https://www.marmiton.org/recettes/recette_financiers_13690.aspx)
