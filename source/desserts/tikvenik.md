# Tikvenik (tarte à la courge bulgare)

## Ingrédients
- pâte filo : 2 paquets (500 g, 20 feuilles)
- courge butternut ou potiron : 500 g
- sucre : 150 g
- noix concassées : 100 g
- cannelle : 1 c. à café
- beurre fondu : 50 ml
- sucre glace : 50 g (pour saupoudrer);

## Temps
- préparation : 20 min
- cuisson : 35-40 min

## Préparation
- Râper la courge (500 g). Mélangez-la avec le sucre (150 g), les noix concassées (100 g) et la faire revenir à feu doux avec un peu de beurre jusqu'à ce qu'elle ramollisse (15 min).
- Laisser refroidir légèrement puis rajouter et la cannelle.
- Prendre une feuille de pâte filo, badigeonner d'un peu de beurre fondu. Répartir une petite quantité de la farce et rouler en cylindre.
- Répéter l'opération avec les feuilles restantes et disposer les rouleaux dans un plat à four beurré.
- Badigeonner les rouleaux avec le reste de beurre fondu.;

## Cuisson
À 180 °C : 35-40 minutes.;

## Finition
- Laisser le tikvenik refroidir, puis saupoudrer de sucre glace avant de servir.;

## Notes
- On peut ne pas précuire la courge.
- Normalement on cuit la courge seule, puis on rajoute le sucre et les noix.
- On peut rajouter des raisins secs.
