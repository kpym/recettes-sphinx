# Riz au lait de coco et mangue

## Ingrédients (pour 4)
- riz : 150 g
- lait de coco : 200 ml
- sucre : 70 g
- mangues (surgelées) : 300 g
- sel : 5g

## Temps
- préparation : 20 min
- cuisson : 20 min

## Préparation
- rincer le riz, puis le laisser gonfler dans l'eau pendant quelques heures
- faire chauffer (bouillon léger) le lait de coco avec le sucre, le sel et 200 ml d'eau (et la vanille)
- verser le riz et faire cuire à feux doux 14 min
- servir le riz tiède avec les mangues froides (parsemé de sésame grillé / zeste de citron vert)

## Remarques
- on peut rajouter de la noix de coco rappée
- on peut parsemer de sésame (grillé) et/ou de zeste de citron vert
- on peut rajouter de la vanille
