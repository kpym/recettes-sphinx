# Baklava « Lelia Mia »

## Ingrédients
- pâte filo (ou phyllo) : 250 g (10 feuilles)
- beurre : 125 g
- noix concassées : 200 g (minimum)
- sucre : 250 g
- eau : 250 ml
- jus de citron : 1/2

## Temps
- préparation : 20 min
- cuisson : 21 min

## Préparation

- Faire fondre le beurre.
- Faire une « lasagne » de taille A4 (20 × 30 cm) avec les feuilles de pâte filo beurrées séparé de 2 (voire 3,4,...) couches de noix concassés.
- Découper en rectangles ou en losanges.
- Verser le reste du beurre fondu : d'abord dans les fentes de la découpe, puis partout ailleurs.
- Cuire. À la sortie du four laisser refroidir.
- En fin de cuisson, préparer le sirop : faire cuire les 250 g de sucre dans 250 ml d'eau pendant 10 minutes. Laisser refroidir un peu (5 min), rajouter le jus d'un demi-citron, puis imbiber le baklava avec ce sirop : d'abord dans les fentes puis partout ailleurs.
- Laisser reposer couvert pendant 1 jour avant de consommer.

## Cuisson

À 210 °C : 21 min

## Version

Au lieu de faire des couches pâte filo / noix, on peut enrouler dans chaque feuille de pâte filo des noix, puis les couper en bouchées et les ranger dans le plat serrés en vertical.

## Notes

- Le citron dans le sirop évite la cristallisation du sucre.
- Sortir un seul morceau avant la fin du repos empêche le reste du baklava de s'imbiber correctement, car le sirop rempli la place libérée.


