# Gâteau coco-choco « Lelia Mia »
_(pour 6 personnes, 20 cm de diamètre)_

## Ingrédients (crème)
- lait : 250 ml
- farine : 25 g
- sucre en poudre : 25 g
- sucre glace : 25 g
- beurre : 80 g
- chocolat noir (Poulin 47 %) : 50 g
- cognac _(ou madère, ou rhum)_ : 7 ml

## Ingrédients (génoise chocolat)
- œufs : 2
- sucre : 75 g
- farine : 25 g
- cacao : 5 g

## Ingrédients (génoise coco)
- blancs d'œufs : 2
- sucre : 70 g
- farine : 15 g
- noix de coco râpée : 70 g

## Crème

Mélanger (au fouet) dans une casserole le **lait**, le **sucre en poudre** et la **farine**. Épaissir à feux très doux pendant 25 min.

_Commencer les génoises pendant que le lait épaissit. Finir la crème lors de la cuisson des génoises._

Ramollir 65 g du **beurre**, faire un beurre pommade. Rajouter la moitié du **sucre glace**. Rajouter l'**alcool**. Mélanger avec la moitié du lait épaissit refroidit pour faire la crème blanche.

Ramollir au micro-onde le **chocolat noir** avec le reste du **beurre** (15 g). Mélanger avec la deuxième moitié du **sucre glace**. Mélanger avec l'autre moitié du lait épaissit pour faire la crème choco.

Réserver au frigo.

##  Génoise chocolat

Battre les **jaunes** d'œufs avec le **sucre** jusqu'à ce que le mélange blanchisse. Monter les **blancs** en neige. Mélanger les blancs et les jaunes. Ajouter la **farine**, mélanger, ajouter le **cacao**, mélanger. Cuire dans une forme de même diamètre que la génoise coco.

### Cuisson

À 170 °C : 35 min

## Génoise coco

Battre les **blancs** en neige. Tout en mélangeant, ajouter le **sucre**, puis la **farine**, puis le **coco**. Mettre dans une forme bien beurrée, de même diamètre que la génoise choco. Rajouter au four, en même temps que la génoise choco.

### Cuisson

À 170 °C : 21 min

## Préparation finale

_(Un peu avant le repas.)_
Étaler la crème blanche sur la génoise au chocolat. Placer la génoise coco _retournée_ par-dessus. Étaler la crème choco sur le dessus du gâteau et sur les côtés.

— Facultatif : décorer avec de la noix de coco et/ou du chocolat râpé et/ou des baies._

## Notes

- Dans la recette originale il y avait des mesures en tasse, en cuillères à soupe/café et en grammes. J'ai tout mis en grammes.
- Le gâteau original est deux fois plus grand.
- Dans la recette originale on fait des génoises doubles, puis on les coupe en deux (avec un fil) et on utilise qu'une moitié (l'autre on la congèle pour une prochaine fois). Donc il fallait adapté le temps et la température de cuisson. On trouve sur internet :
  - génoise haute (4 œufs) : 25–40 min à 150–170 °C
  - génoise fine (2 œufs) : 10–15 min à 210–220 °C

  La cuisson était :
  - génoise (double) coco : 15 + 15 min à 220 °C + 160 °C
  - génoise (double) chocolat : 40 min à 200 °C

