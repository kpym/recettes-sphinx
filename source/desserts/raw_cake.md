# Raw Cake (Mango)

## Ingrédients

### base

- noix de pécan (ou autres noix, ou amandes) : 200 g
- dattes (ou autres fruits secs) : 300 g
- huile de coco : 3 c.-à-s. (≈ 30 g)
- sel : 3 pincés

### nappage (mangue)

- noix de cajou : 150 g
- mangue (ou framboises, figues...) : 200 g
- huile de coco : 100 g
- sirop d'érable (d'agave) : 50 g
- vanille

## Préparation

Laisse tremper les noix de cajou quelques heures dans de l'eau.

On hache dans le blender les ingrédients de la base. On remplit le fond d'une forme tapissée de papier cuisson et on laisse au congélateur le temps de préparer le nappage.

On fait fondre le beurre de coco dans le micro-onde. On mélange le tout dans le blender. On verse sur la base et on remet au congélateur 30 min.

On garde au congélateur ou au frigo.

## Notes

- On peut rajouter de la noix de coco rappée dans la base.
- Pour faire au chocolat (et pas à la mangue) on remplace dans le nappage les 200 g de mangue par :
  - avocat : 1/2 (≈ 100 g)
  - cacao : 35 g
  - eau : 70 ml
  On peut aussi remplacer l'huile de coco par de l'huile de cacao.

## Références

- https://veganbell.com/no-bake-vegan-mango-cheesecake-recipe/
- https://youtu.be/SAmVFMHE4XM
