# Strudel

## Ingrédients

- pommes : 2-3
- sucre : 75 g
- raisins secs : 75 g
- noix : 75 g
- jus d'un demi-citron
- noix de muscade
- cannelle
- 5 feuilles de pâte filo (ou éventuellement pâte feuilletée pur beurre)
- chapelure
- un peu de beurre
- sucre glace

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

On mélange les pommes coupées en petits dés, le sucre, les raisins secs, les noix, le jus de citron, la noix de muscade et la cannelle. On enroule dans 5 feuille de pâte filo beurrées et séparées par de la chapelure. On fait cuire.

## Finition

Une fois sorti du four, beurrer le dessus puis parsemer de sucre glace.

## Cuisson

À 190 °C : 35 min

## Finition

Parsemer de sucre glace.
