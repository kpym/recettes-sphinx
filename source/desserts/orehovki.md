# Orehovki / Ореховки

## Ingrédients

- noix en poudre : 150 g
- sucre (roux) : 150 g
- blanc d'œuf : 2
- maïzena : 10 g (2 c.c.)
- vanille
- jus de citron

## Temps

- préparation : 10 min
- cuisson : 20 min

## Préparation

Monter le blanc en neige avec un peu de sucre. Incorporer le sucre, les noix en poudre, la maïzena, la vanille et les quelques gouttes de citron. Former des petits tas sur la plaque de cuisson et enfourner dans le four chaud.

## Cuisson

À 150–160 °C : 20 min (à surveiller)
