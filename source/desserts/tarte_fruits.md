# Tarte aux fruits

## Ingrédients

### pour la pâte sablée
- beurre : 100 g
- sucre : 100 g
- farine : 200 g
- jaune d'œuf : 1

### pour la ganache
- beurre : 100 g
- sucre : 100 g
- œufs : 2
- voir les notes pour d'autres produit éventuels

### fruits
- pomme, pruneaux, rhubarbe, ... : 350-700 g (congelés c'est possible)

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

Faire une pâte sablée (voir la recette). Pendant qu'elle cuit, on fait la ganache :
- 1 min le beurre dans le micro-ondes (pour devenir liquide)
- on rajoute le sucre, on mélange
- on rajoute les œufs, on mélange
- on dispose les fruits (sans trop les décongeler) sur la pâte sablée chaude
- on verse la ganache dessus

## Cuisson

À 190 °C : 11 min la pâte + 25-35 min avec le reste.

## Note

On peut aussi :
- remplacer la pâte sablée par une pâte feuilletée _pur beurre_ achetée dans le commerce.
- remplacer le beurre par 20 cl de crème fraîche
- mettre un œuf ou un blanc de plus
- si les fruits sont trop juteux mettre de la chapelure.
- rajouter 100 g ou moins de poudre d'amandes ou de noix de coco
- rajouter 25 g de maïzena
