# Glace aux marrons de « Monique »

## Ingrédients

- jaunes d'œufs : 4
- crème de marrons : 250 g
- sucre : 90 g
- crème fouetté 30 % : 250 ml

## Temps

- préparation : 20 min
- congélation : 5 h

## Préparation

- Faire mousser les jaunes avec le sucre, puis cuire au ban marie. Laisser refroidir.
- Ajouter la crème de marrons.
- Ajouter la crème fouettée.
- Verser dans un moule de 1 litre et laisser au congélateur au moins 5 heures.

