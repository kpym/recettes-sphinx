# Tarte Tatin

## Ingrédients

### pour la pâte sablée
- beurre : 100 g
- sucre : 100 g
- farine : 200 g
- jaune d'œuf : 1

### pour le caramel
- beurre : 100 g
- sucre : 100 g

### fruits
- pomme golden : 10

## Temps
- préparation : 30 min
- cuisson : 35 min

## Préparation

On épluche et on coupe les pommes (en moitiés ou en cubes). On prépare la pâte sablée. On fait du caramel dans le plat à tarte sur une plaque (100 g de sucre + 100 g de beurre, environs 5 min). On rajoute les pommes (si des moitiés on les met à la verticale). On continue à cuire encore 5 min à l’étouffé. On pose la pâte sablée en faisant entrer les bords et en perçant une cheminée au milieu.

## Cuisson

À 180 °C : 35 min

## Présentation

À servir tiède avec glace à la vanille, crème fraîche ou chantilly.
