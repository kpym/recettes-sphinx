# Tarte aux noix

## Ingrédients

### pour la pâte sablée
- beurre 1/2 sel : 100 g
- sucre : 100 g
- farine : 200 g
- jaune d'œuf : 1

### pour la ganache
- beurre : 70 g
- sucre : 100 g
- noix concassées : 100 g
- œufs : 1 + 1 blanc

### pour la couverture (noix + caramel)

- cerneaux de noix (pécan, de Brésil, ...) : 300 g
- sucre : 100 g
- crème fraîche : 50 ml
- beurre 1/2 sel : 20 g

## Temps
- préparation : 40 min
- cuisson : 30 min

## Préparation

### La pâte sablée

Faire une pâte sablée (voir la recette dans « Autres recettes »). La cuire 11 min à 190 °C.

### La ganache

Pendant que la pâte cuit, on fait la ganache :
- concasser (broyer) les 100 g de noix
- 1 min le beurre dans le micro-ondes (pour devenir liquide)
- on rajoute le sucre, on mélange
- on rajoute les œufs, on mélange
- on rajoute les noix concassées, on mélange

On fait cuire 7 min à 190 °C, puis on la sort, on dispose les 300 g de noix diverses et l'enfourne pour encore 7 min à 190 °C. La laisser au four le temps de préparer le caramel.

### Le caramel

Après la fin de la cuisson de la ganache :
- mettre la crème fraîche à chauffer
- faire brunir le sucre dans une casserole
- en dehors du feu, verser la crème fraîche brûlante sur le sucre toute en remuant
- rajouter ensuite le beurre

Couvrir la tarte avec le caramel.

## Cuisson

À 190 °C : 25 min (11 min la pâte + 7 min avec le ganache + 7 min avec les noix).

## Notes

- Attention à ne pas prendre des noix rances.
- Il est préférable de mélanger les noix (normaux, de Brésil, pécan, de cajou... et même des amandes).
- J'ai diminué la quantité de caramel de 1/3, mais même moins ça devrait suffire.
