# Banoffee

## Ingrédients

- biscuits (Sables de Flandres) : 300 g
- sucre roux : 45 g
- beurre (½ sel) : 85 g
- lait concentré sucré : 1 boite
- bananes : 4
- crème 30 % pour chantilly : 30 cl

## Temps
- préparation : 5 min + 20 min
- cuisson : 40 min

## Préparation

Caramel [en avance] : Faire cuire la boite de lait concentré, sans l'ouvrir, dans une cocote-minute pendant 40 minutes.

Fondre le beurre au micro-ondes (1 min). Écraser les biscuits, les mélanger avec le beurre fondu et le sucre. Tasser dans un plat à fond amovible (ou dans un cercle), couper les bananes par-dessus, puis mettre au congélateur 15 min. Couvrir du caramel. Mettre au frigo. Servir couvert de chantilly.

## Cuisson

Cocote-minute : 40 min (le caramel)

