# Crêpes (pâte)

## Ingrédients
_(5 crêpes par 100 g de farine)_

- œufs
- farine : œufs × (70–100) g
- lait : farine × 2
- sel : œufs × 1 g

_Exemple pour 12–13 crêpes : 3 œufs, 250 g de farine, 500 ml de lait et 3 g de sel._

### optionnel

- sucre : ≈ œufs × ½ c. à s.
- huile : ≈ œufs × ½ c. à s.
- bière : remplacer 10–20% du lait
- maïzena : remplacer 10–20% de la farine
- levure chimique : ≈ œufs × 3 g
- vanille
- rhum

## Préparation

Mélanger le tout et laisser reposer la pâte 1 h.
