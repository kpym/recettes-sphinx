# Shortbread au beurre salé

## Ingrédients

- beurre 1/2 sel : 250 g (dur coupé en petits dés)
- sucre : 85 g (+ pour la finition)
- farine : 330 g

## Temps
- préparation : 20 min
- cuisson : 50 min

## Préparation

Mélanger tous les ingrédients (dans l'ordre beurre, sucre, farine) pour obtenir un mélange sableux. Déposez en pressant la pâte dans un moule rond. Faites cuire 50 min à 150 °C.

## Finition

Sortez du four, découpez en triangles et parsemer de sucre tant qu'il est chaud.
Laissez refroidir dans le plat.


## Cuisson

À 150 °C : 50 min

## Variante

On peut le faire au noix (normaux, de pécan ou autres) avec les ingrédients suivants :

- beurre 1/2 sel : 150 g (dur coupé en petits dés)
- sucre : 100 g
- farine : 250 g
- noix en poudre : 150 g
