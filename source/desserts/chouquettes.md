# Chouquettes

## Ingrédients (pour 350 g = 225 g + 125 ml)

- eau : 125 ml _(on peut remplacer 35 ml par du lait)_
- sel : 3 pincées
- sucre : 4 g
- beurre : 50 g
- farine : 70 g
- œufs : 2
- jaune d'œuf : 1 _(pour badigeonner)_
- sucre perlé (dit en grains) _(pour le dessus)_

## Temps
- préparation : 45 min
- cuisson : 25-30 min

## Préparation

### La pâte à choux

Préchauffer le four à 210 °C (th. 7).
Mettre à bouillir les liquides avec le sel, le sucre et le beurre.
Tamiser la farine, puis quand le beurre est fondu l'ajouter en une fois, hors du feu, et remuer vivement. Cuire la panade obtenue à feu moyen encore quelques minutes pour bien la dessécher.
Refroidir (légèrement) ensuite la préparation, puis ajouter les œufs 1 par 1 (jusqu'à obtenir un ruban un peu cassant lorsqu'on lève la cuillère). La pâte est prête lorsqu'elle tombe de la cuillère en une pâte ferme mais souple.
À l'aide d'une poche à douille, coucher le mélange en boules de 3 cm de diamètre, les dorer avec un jaune d'œufs passé au pinceau, et à la fin les couvrir de sucre perlé. Les cuire 21 min : 7 min dans un four à 210 °C chaleur tournante, 7 min à 190 °C et 7 min 150 °C. À chaque changement de température, ouvrir la porte très rapidement pour laisser échapper l'humidité (mais sans refroidir le four). Laisser au four 10 min pour continuer à sécher la pâte et éviter que les choux ou les éclairs ne dégonflent.

## Notes
- Pour que la pâte à choux ne retombe pas (dû à l'humidité enfermée dedans) il y a différentes solutions :
    - bien cuire (10 min de plus) la pâte avant d'incorporer les œufs pour qu'elle se dessèche ;
    - cuire à four tout légèrement ouvert (coincé une cuillère);
    - cuire à four fort en première (ou en deuxième) moitié de cuisson pour « durcir » la pâte (il faut faire une pâte foncée);
    - à mi-cuisson, ouvrir la porte très rapidement pour laisser échapper l'humidité (mais sans refroidir le four).
- Sans le sel la pâte est fade.
- La recette originale disait cuisson 21 min à 210 °C, mais la pâte brunissait trop, encore plus que pour les éclaires, dû au sucre perlé.
