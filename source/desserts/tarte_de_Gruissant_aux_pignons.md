# Tarte de Gruissan aux pignons de pin

Il s'agit une sorte de galette des rois avec de la meringue et des pignons pardessus.

## Ingrédients

- pâtes feuilletées pur beurre : 2
- œufs : 3
- sucre : 210 g
- farine : 40 g
- lait : 350 ml
- poudre d'amandes : 125 g
- pignons de pins : 70-100 g

### optionnel

- vanille ou Cognac _(pour la crème)_
- sel _(pour la meringue)_
- maïzena _(pour la meringue)_
- citron _(pour la meringue)_
- sucre glace _(pour la déco)_

## Temps
- préparation : 40 min
- cuisson : 30 min

## Préparation

- On fait cuire à blanc (30 min à 190°C) les deux pâtes feuilletées fortement piquées avec la fourchette (pour éviter les boursouflures).
- On prépare une crème pâtissière « facile » : On tiedie le lait. On blanchit 2 jaunes + 1 œuf entier + 70 g de sucre. On rajoute la farine, un peu de sel, puis le lait tiède. On chauffe sur feu doux et on tourne tant qu'elle n'a pas fini d'épaissir (≈ 10 min). Hors du feu on rajoute la poudre d'amande et si nécessaire un peu de lait _(vanille, Cognac)_.
- On étale la crème entre les deux disques de pâtes.
- On prépare une meringue avec les 2 blancs + 140 g de sucre (une pincée de sel, un peu de maïzena et un peu de citron).
- On couvre avec la préparation blanche le gâteau et on saupoudre avec les pignons de pins.
- On cuit 2h à 90°C. En ouvrant tous les 30 min pour faire sortir l'humidité.
- On saupoudre de sucre glace avant de servir.

## Cuisson

À 190°C : 30 min la pâte.
Sur feu doux : 10 min le lait + 10 min la crème.
À 90°C : 2h pour la meringue.

## Notes

- On peut rajouter de la vanille et/ou du cognac dans la crème.
- On peut remplacer (une partie de) la farine par de la maïzena pour la crème pâtissière.
- D'habitude dans la meringue on rajoute un peu de citron et un peu de maïzena.

## Source

- https://youtu.be/QqVaV2qS1eg
