# Biscuits bicolores de Noël

## Ingrédients

- beurre : 250 g
- sucre : 250 g
- farine : 500 g
- cacao : 3 cuillères
- œuf : 2 + 1 blanc
- levure chimique : 1/2 sachet (4 g)
- sel


## Temps
- préparation : 25 min
- cuisson : 20 min

## Préparation

On mélange le beurre, le sel, le sucre, les œufs, la farine avec la levure. On sépare la pâte en deux. On rajoute les 3 cuillères de cacao dans une des moitiés. On laisse reposer les deux pâtes, enrobés dans du film fraîcheur, au frigo 1-2h. On bat le blanc en neige. On étale les deux pâtes pour faire des rectangles. On les superpose collé par le blanc en neige étalé entre les deux. On les enroule, enrobés de films fraîcheur, et on laisse la pâte encore 1-2h au frigo. On coupe en rondelles et on cuit.
Attention : ils gonflent.

## Cuisson

À 180 °C : 15 min

