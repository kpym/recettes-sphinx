# Gâteau trois chocolats

## Ingrédients

## Génoise

- œufs : 2
- sucre : 50 g
- farine : 15 g
- poudre d'amandes : 15 g
- sel : 1 pincée

### Croquant

- pralinoise (Poulin) : 140 g
- crêpes dentelles (Gavottes) : 70 g

### Mousses

- chocolats : 90 g noir (pas plus de 50 %) + 90 g lait (à cuisiner) + 90 g blanc (à cuisiner)
- lait : 3 × 70 g
- gélatine : 4 1/2 feuilles (3 × 3 g = 9 g)
- crème liquide : 3 × 14 cl

## Temps

- préparation : 2 h (car il y a ≈30 min entre les différents mousses)
- cuisson : 11 min

## Préparation

Mettre la jatte métallique au congélateur pour la préparation de la chantilly.

### Support

**Génoise :** Battre les **jaunes** d'œufs avec le **sucre** jusqu'à ce que le mélange blanchisse. Monter les **blancs** en neige. Mélanger les blancs et les jaunes. Ajouter la poudre d'**amande**, mélanger, ajouter la **farine** et le **sel**, mélanger. Verser dans un moule silicone légèrement graissé ou sur un tapis silicone graissé, il faut que la pâte fasse un disque un peu plus grand que le disque avec lequel vous aller faire le gâteau (24/26 cm de diamètre). Cuire 11 min à 180°. Laisser refroidir avant de démouler.

**Croquant :** Faire fondre votre **pralinoise** en morceaux au micro-ondes ou au bain marie. Émietter les **crêpes dentelles** et les déposer sur la pralinoise fondue. Mélanger.

Couper votre génoise avec votre cercle si elle est plus grande que le cercle, disposer le cercle avec la génoise sur le plat de service, mettre du rhodoïd, puis tasser ce mélange au fond. Réserver au frais.

### Mousse

Dans l'ordre (noir, lait, blanc), et laissez prendre au moins 2 h au réfrigérateur (30 min au congélateur) avant de couler la mousse suivante.\
Mettre la **gélatine** à réhydrater dans un récipient d'eau froide environ 10 min. Pendant ce temps, faire fondre votre **chocolat**. Faire chauffer le **lait** 30 s au micro-ondes à pleine puissance puis y mettre la **gélatine** essorée, mélanger. Verser en 3 fois le **lait** sur le **chocolat** fondu, bien mélanger à chaque fois, au départ le mélange est épais, au final il doit être homogène et liquide si vous avez correctement mélangé. Laisser refroidir _(au frigo)_. Monter votre **crème** en chantilly mousseuse. Lorsque le chocolat est tiède _(pas avant)_, ajouter un peu de chantilly, mélanger puis ajouter en 3 fois le reste délicatement. Verser cette mousse et laisser au frigo (congélateur) avant de couler la mousse suivante.

## Cuisson

À 180 °C : 11 min pour la génoise

## Notes

- Pour la génoise : ne surtout pas mettre la farine et la poudre d'amande avant de mélanger les blancs en neige et les jaunes avec le sucre.
- Pour les mousses : ne surtout pas mélanger la chantilly avec le chocolat tant qu'il est chaud, sinon la chantilly retombe.
- En général je ne laisse que 15-20 minutes au congélateur entre les couches de mousse, le temps de préparer la suivante.
- En général dans la génoise il y a plus de farine et moins de sucre (les deux de même quantité).

- [La source principale](http://www.lesgourmandisesdenemo.com/entremet-3-chocolats)
- [Une autre version](http://www.elle.fr/Elle-a-Table/Recettes-de-cuisine/3-chocolats-sur-un-croustillant-pralinoise-2031516)
- [Encore une version](https://gateaux-chocolat.fr/recette/recette-bavarois-au-trois-mousses-au-chocolat/)
- [Génoise classique](https://www.atelierdeschefs.fr/fr/recette/4925-biscuit-genoise.php)
