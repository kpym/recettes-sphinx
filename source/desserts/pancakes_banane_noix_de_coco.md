# Pancakes à la banane et noix de coco

## Ingrédients
- bananes très mûres (pourries) : 2
- œufs : 2
- farine : 140 g
- sucre (roux) : 70 g
- lait de coco : 100 g
- noix de coco râpée : 14 g
- levure chimique : 4 g
- sel : 3 g
- vanille : quelques gouttes
- beurre (pour la cuisson)

## Temps
- préparation : 14 min
- cuisson : 14 min

## Préparation

Écraser les bananes. Mélanger avec les œufs et les battre. Rajouter le sucre, le sel, la levure, le lait de coco, la vanille ; mélanger. Rajouter la farine et la noix de coco râpé ; mélanger.

Cuire à la poêle modérément chaude avec du beurre.
