# Pavlova

## Ingrédients

### Meringue

- blancs d'œufs : 180 g (≈6 œufs)
- sucre en poudre : 300 g
- sel : 1 pincée
- jus de citron : 1 cl (≈2 c.c.)
- Maïzena : 5 g (≈1 c.c.)

### Crème

- mascarpone : 150 g
- crème 30 % pour chantilly : 30 cl
- sucre : 25 g
- vanille

### Fruits (exemples)

- pêches / nectarines
- framboises / fraises
- mangue / bananes

## Temps
- préparation : 2h30
- cuisson : 1h30 - 2 h

## Préparation

### Meringue

Battre les blancs en neige très ferme avec le sel. Ajouter le sucre peu à peu tout en battant. Ajouter le citron, battre, puis la Maïzena, battre encore. Le mélange doit être très ferme et brillant.

### Crème chantilly

Mélanger le mascarpone et la crème dans une jatte refroidie au congélateur. Battre, rajouter le sucre et la vanille, continuer à battre .

## Cuisson de la meringue

À plus de 100 °C la meringue cuit (devient jaunâtre), à moins de 100 °C elle sèche. Il faut ouvrir le four toutes les 20 min pour faire sortir l'humidité.

Disposer dans un disque avec une douille en rajoutant plus sur le bord.

- 1h15 à 120 °C
- 1h30 à 100 °C
- 2 h à 90 °C

## Finition

On remplit la meringue avec la crème chantilly, puis on dispose dessus les fruits.

## Notes

- Au début j'avais mis × 2 (360 g) de sucre, mais je trouve qu'elle était trop sucrée. La plupart des recettes utilisent entre × 1.5 et × 2, mais il y a des recettes (Larousse des desserts) où c'est × 0.9 !
- Il y a des recettes :
    - dans lesquelles on bat seulement avec 1/3 du sucre. Et les autres 2/3 on les incorpore délicatement.
    - qui rajoute environ 1 h (c'est-à-dire 3 h) à la cuisson 95 °C de la meringue.
- On peut remplacer le mascarpone par du yaourt grec.
- On peut couvrir la meringue avec du chocolat fondu. Et si on met deux disques on obtient une sorte de « vacherin ».
- Dans les ingrédients de la meringue, on peut remplacer le citron par du vinaigre blanc (d'alcool ou de vin blanc).
