# Cannelés

## Ingrédients
_(pour 24 cannelés ≈ 2 kg ≈ 1500 ml)_

- lait cru frais : 1 litre
- vanille : 2 gousses
- œufs : 6 (4 entiers + 2 jaunes)
- beurre fondu : 25 g
- farine : 250 g
- sucre glace : 450 g
- sel : 3 g

## Temps

- préparation : 20 min (pâte) + 24 h (repos)
- cuisson : 70 min

## Préparation

Faire bouillir le lait (1 l) avec les gousses (2) éventrées et bien ouvertes afin que toute la vanille se diffuse. Pendant ce temps, battre les œufs (4 + 2 jaunes). Verser le lait chaud sur les œufs. Mélanger. Ajouter le beurre fondu. Mélanger. Laisser le mélange tiédir. Pendant ce temps, mélanger la farine (250 g), le sucre glace (450 g) et le sel (3 g). Verser le liquide refroidi sur les poudres. Laisser reposer 24 h _(les grumeaux fondent pendant le repos)_. Verser dans des moules à cannelés en silicone (3 × 8 = 24 pièces) en laissant 5 mm, car ils gonflent à la cuisson.

## Cuisson

- 220 °C 10 min, puis 180 °C pendant 1 h.
