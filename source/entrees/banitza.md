# Banitza

## Ingrédients
- pâte filo : 10-15 feuilles
- yaourt : 1 pot
- œufs : 3
- fêta : 200 g
- beurre : ≈100 g

## Temps
- préparation : 25 min
- cuisson : 35 min

## Préparation

Pour faire la farce : on mélange le yaourt + 3 œufs + 200 g de fêta.
On fait fondre le beurre.
On étale du beurre fondu avec un pinceau sur chaque feuille de pâte filo, on met 2-3 cuillères à soupe de la farce et on enroule.
Au four 35 min à 190 °C.

## Remarque

On peut remplacer la pâte filo (ou phyllo) par des feuilles de brick
