# Soupe de courgettes

## Ingrédients

### pour la préparation
- courgettes : 3
- sel
- poivre
- persil

### pour la béchamel
- lait : 25 cl
- beurre : 20 g
- farine : 20 g

### pour le service
- yaourt ou crème fraîche


## Temps
- préparation : 25 min
- cuisson : 35 min

## Préparation

Faire cuire les courgettes épluchées et coupées en grosses rondelles.

Pendant ce temps préparer la béchamel au micro-ondes ou de la façon traditionnelle : faites fondre le beurre dans une casserole à fond épais ; ajoutez la farine et remuez avec une cuillère de bois sans laisser colorer ; la farine doit juste épaissir ; versez le lait progressivement, sans cesser de remuer, jusqu'à ce que la sauce épaississe.

Mixer le tout, rajouter la sauce béchamel. Assaisonner. On sert avec du yaourt ou de la crème fraîche au fond des assiettes.
