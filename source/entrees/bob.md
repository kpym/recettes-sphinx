# Bob (soupe bulgare de haricots blancs)

## Ingrédients

### pour la soupe
- haricots blancs : 1 tasse
- oignon : 1
- poivrons : 2
- carotte : 1
- céleri (tiges, tête ou feuilles) : une poignée
- [menthe verte](https://bg.wikipedia.org/wiki/%D0%94%D0%B6%D0%BE%D0%B4%D0%B6%D0%B5%D0%BD) : 1 cuillère à café
- [sarriette](https://bg.wikipedia.org/wiki/%D0%A7%D1%83%D0%B1%D1%80%D0%B8%D1%86%D0%B0) : 1 cuillère à café
- sucre : 1 cuillère à café
- huile : 50 ml

### pour le roux
- huile : 100 ml
- farine : 1 cuillère à soupe
- paprika : 1 cuillère à soupe

## Temps

- préparation : 30 min + 10 min
- cuisson : 50 min + 15 min

## Préparation
Il est toujours préférable de faire tremper les haricots dans de l'eau froide la veille. On fait cuire (≈50 min à la cocotte)  dans 6 tasses d'eau avec un peu d'huile mais sans les herbes. En fin de cuisson on rajoute le sel, la sarriette et la menthe verte. Pendant que les haricots continuent de cuire à feux doux, dans une petite casserole on fait bouillir environ 50 ml d'huile, puis on rajoute 1 cuillère à soupe de farine on le fait dorer. On retire la casserole du feu et on rajoute la cuillère à soupe de paprika, puis on verse [le roux](https://bg.wikipedia.org/wiki/%D0%97%D0%B0%D0%BF%D1%80%D1%8A%D0%B6%D0%BA%D0%B0) dans les haricots et on fait bouillir encore 5 minutes.

## Version « yahniya »
Pour faire une version « plat », et non soupe, après la cuisson on enlève (filtre) une grosse partie du liquide et on fait cuire au four 15 min.
