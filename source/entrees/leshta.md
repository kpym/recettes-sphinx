# Leshta (soupe de lentilles vertes)

## Ingrédients
- lentilles vertes : 1 tasse
- poireau (ou oignon) : 1
- carottes : 3
- ail : 1 tête
- [sarriette](https://bg.wikipedia.org/wiki/%D0%A7%D1%83%D0%B1%D1%80%D0%B8%D1%86%D0%B0)

### pour le roux

- huile : 100 ml
- farine : 1 cuillère à soupe
- paprika : 1 cuillère à soupe

## Temps

- préparation : 15 min + 10 min
- cuisson : 10 min + 5 min

## Préparation
On fait cuire (10 min à la cocotte) tous les ingrédients dans 7 tasses d'eau avec un peu d'huile. En même temps on prépare [le roux](https://bg.wikipedia.org/wiki/%D0%97%D0%B0%D0%BF%D1%80%D1%8A%D0%B6%D0%BA%D0%B0) comme pour le « bob », on le rajoute en fin de cuisson et on fait cuire encore 2 min.
