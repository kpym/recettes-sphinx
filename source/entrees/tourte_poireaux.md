# Tourte aux poireaux « Sneja »

## Ingrédients

### Farce
- poireaux : 500 g
- fêta : 100 g
- œufs : 1

### Pâte
- farine (T65) : 400 g
- huile d'olive : 70 g (≈ 78 ml)
- lait : 140 ml
- œuf : 1
- levure chimique : 5 g (½ sachet)
- sel

## Temps
- préparation : 20 min
- cuisson : 28 min

## Préparation

### La farce

On coupe les poireaux et on les fait cuire (à la vapeur ou revenus à la poêle). On émiette la fêta, on la mélange avec l'œuf et les poireaux. On réserve.

### La pâte

On mélange bien les liquides : œuf, lait, huile. On mélange la farine, la levure chimique et le sel. On fait un puis dans la farine où on verse petit à petit le liquide, tout en mélangeant avec la main, pour former à la fin une boule qu'on travaille un peu. On partage la pâte en 40/60. On étale la grande partie en cercle (⌀26–28 cm) et on tapisse la forme (⌀22–24 cm) en remontant sur les côtés. On remplit avec la farce, puis on couvre avec le reste de la pâte étalé en cercle. On fait un petit troue au milieu.

## Cuisson

À 190 °C : 28 min.

## Remarque

Dans la recette originale de Снежа il y a plus d'huile (et pas l'huile d'olive), moins de farine et il n'y a pas d'œuf dans la farce.
