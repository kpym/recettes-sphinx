# Kyouftéta (boulettes) de courgettes

## Ingrédients

### pour la préparation
- courgettes : 3
- œufs : 2
- persil : une cuillère de soupe (ou plus)
- sel
- poivre
- farine

### pour le service
- yaourt
- ail

## Temps
- préparation : 25 min
- cuisson : 35 min

## Préparation

Éplucher et râper les courgettes, puis les faire dégorger (saler, attendre, à la fin jeter l'eau qui se libère). Mélanger avec les 2 œufs et de la farine : pour obtenir une consistance pâteuse. Rajouter le persil, le sel et le poivre. Les faire frire, puis les servir avec du yaourt aillé.
