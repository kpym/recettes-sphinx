# Bretzels

## Ingrédients (pour 8 bretzels)

### pour la pâte
- farine : 500 g
- lait : 200 ml
- eau : 100 ml
- beurre : 30 g
- sel : 7 g
- levure boulangère : 10 g sèche ou 20 g fraîche
- gros sel

### pour le bain de saumure
- eau : 2 l
- bicarbonate de sodium : 40 g
- gros sel : 15 g

## Temps
- préparation : 50 min (+ 1h30 de levage)
- cuisson : 30 sec dans la saumure + 14 min au four

## Préparation

### À la main
On mélange le lait et l'eau. On fait un puits dans la farine, on met le sel au bord de la jatte, la levure dans le puits, avec un peu de lait/eau. On commence à malaxer le milieu et plus la boule se forme, plus on rajoute (un peu) du lait/eau. À la fin on rajoute le beurre ramolli.

### À la machine
On verse le lait dans la jatte + la moitié de l'eau. On rajoute 300 g farine et la levure. On fait tourner à 1-2. Pendant ce temps on fait fondre le beur et on y rajout le sel, puis on verse. On fait tourner. On rajoute le reste de la farine (200 g) et le reste de l'eau (50 ml). On fait tourner.

---

Laisser reposer la pâte _(badigeonnée d'huile d'olive)_ pour qu'elle double de volume (≈ 1h30).

Préchauffer le four à 230 °C.
Faire bouillir de l'eau dans une casserole avec le sel et le bicarbonate.
Séparer la pâte en 8 parties et former les bretzels ou des petites baguettes.
Puis les plonger un par un dans l'eau bouillante, attendre qu'ils remontent (≈ 30 sec) et égoutter sur de l'essuie-tout.
Parsemer de gros sel.

## Cuisson

À 230 °C : 14 min.

## Notes
- On peut mettre que du lait.
- On trouve de la cuisson 15 min à 200 °C ou 12 min à 230 °C dans les recettes.
- En cas de levure fraîche on met le double et on peut mette un peu de sucre pour l'activation.
- Dans la version bulgare de « варени гевреци » on met miel et sucre à la place de bicarbonate de sodium et sel dans l'eau et il n'y a pas du beurre ni de lait.
- On peut couvrir avec sésame, pavot, graines de courge... avant d'enfourner.

## Sources

- https://youtu.be/BW0Z_u9o53s
- https://cuisine.journaldesfemmes.fr/recette/316157-bretzels-sales
