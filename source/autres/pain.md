# Pain

## Ingrédients
- farine : 500 g
- eau tiède : 250-300 ml
- levure sèche boulangère : 2 sachets = 10 g
- sel : 10-20 g

## Temps
- préparation : 15 min + 15 min
- cuisson : 15 min

## Préparation

On fait un puis dans la farine, on met le sel au bord de la jatte, la levure dans le puis, avec un peu d'eau. On commence à malaxer le milieu et plus la boule se forme, plus on rajoute (un peu) d'eau. Une fois la pâte prête on la laisse reposer quelques heures à température ambiante.

On prépare les pains en essayant de mélanger le moins possible la pâte (en préservant autant que possible la fine croûte). On laisse reposer le temps de préchauffer le four à 250°. On fait cuire avec un récipient d'eau dans le four 15 minutes (à max).
