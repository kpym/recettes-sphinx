# Génoise

## Ingrédients
- œuf : 4
- sucre : 120 g
- sucre-vanille : 1 sachet
- farine : 25 g
- maïzena : 75 g
- levure chimique (alsacienne) : 1/2 sachet
- sel : 1 pincée

## Temps
- préparation : 20 min
- cuisson : 20 min

## Préparation
- Battre les jaunes avec les sucres (semoule + vanille).
- Rajouter les poudres (farine, maïzena, levure, sel).
- Monter les blancs en neige. Puis, incorporez-les au reste.
- À cuire sur un tapis de cuisson en silicone (dans un cercle).

## Cuisson

À 150 °C : 20 min

## Note
- Source principale : [Desserts en dessins](https://i.imgur.com/U8omvKh.png)
