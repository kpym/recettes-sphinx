# Cumin vs Carvi

## Carvi
Ким [bg], Kümmel [de], Caraway [en]

- plus foncé
- plus étoilé
- sans tiges au bout des graines
- utilisation dans l'Europe du Nord (utilisé dans la pâtisserie hongroise et allemande et dans certains biscuits anglais)
- anisé
- en France : souvent vendu en graines
- le « Guda au cumin » est au carvi
- ne se mélange pas avec autres parfin

## Cumin
Кимион [bg], Krenzkümmel [de], Cumin [en]

- plus claire
- avec de tiges au bout de certaines graines
- utilisation dans la méditerranée, Inde et le proche orient
- en Bulgarie : pour la viande
- en France : souvent vendu en poudre, s'utilise avec courge, carottes...
- se mélange bien avec джоджен, чубрица...
