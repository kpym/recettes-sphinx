# Confiture de pommes

## Ingrédients (pour 1 petit pot)
- pomme : 2 (≈250 g)
- sucre à confiture : 50 g (1/5 des pommes)
- sucre : 100 g (2/5 des pommes)

## Temps

- préparation : 15 min + 15 min
- cuisson : 4 min

## Préparation
On coupe la pomme en petit morceau (surtout ne pas les mixer ou rapper), on mélange avec le sucre et on laisse reposer plusieurs heures. On fait cuire à feu vif pendant 4 minutes en remuant. On mixe et on verse dans les pots tant que c'est chaud.
