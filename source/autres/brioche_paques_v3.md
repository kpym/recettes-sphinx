# Brioche de Pâques bulgare (Kozounak)

## Ingrédients (750 g de farine → 1570 g de pâte, pour 1 grand et 1 petit)

- farine forte : 750 g
- sel : 15 g
- sucre : 200 g
- levure : 37 g frêche ou 14 g de levure sèche
- œufs : 4 (≈ 220 g)
- lait : ≈ 160 g (lait + œufs = 380 g)
- gras : 180 g
    - beurre : 130 g
    - saindoux : 40 g
    - huile : 10 g
- écorce d'un ½–1 citron (≈ 7–14 g)
- vanille (≈ 10 g)

### Pour la farce

- raisins secs, abricots secs, noix, amandes, confitures...
- on peut faire une nougatine
 
## Préparation (au robot)

### Pétrissage au robot

1. Mettre dans la jatte du robot les 4 œufs.
1. Compléter avec du lait jusqu'à 380 g.
1. Rajouter la levure.
1. Rajouter le sucre. Mélanger pour que le sucre se dilue. Laisser reposer.
1. Tamiser la farine.
1. Faire un mélange beurre (30 g), saindoux (40 g) et huile (10 g). Ramollir ce mélange au micro-onde.
1. Rajouter le sel (15 g) au gras et touiller pour faire une pommade.
1. Rajouter la vanille (≈ 10 g), l'écorce de citron (≈ 10 g) dans la jatte du robot.
1. Rajouter la farine (750 g) en plusieurs fois. Mélanger, puis battre au robot 5 min _(jusqu'à ce que la pâte devienne élastique)_.
1. Laisser reposer 10–15 min.
1. Incorporer le gras en battant ≈ 20 min.

### Pousse

- On laisse la pâte se reposer 30 min sous un torchon.
- On fait quelques pliures, on forme une boule et on laisse pousser encore 1 h filmé.
- On réserve au frigo une nuit (ou quelques heures) filmé pas trop serré.
- On forme les brioches et on laisse monter environ 2 h, dont 1 h au four programme « levage ».

### Cuisson

- On préchauffe le four à 180 °C.
- On badigeonne les brioches avec un mélange de blanc d'œuf et du lait, on rajoute des noix et du sucre pardessus.
- On fait cuire **sans chaleur tournante en bas du four**.
- La petite brioche de l'ordre de 35 min, la grande de l'ordre de 45 min.

## Temps

- préparation : 5h30 (60 + 30 + 90 + 30 + 90 + 30)
- cuisson : 40 min

## Notes

- Il faut utiliser une farine forte (protéines>12 %, W>300). De préférence Manitoba ou de Gruau.
- Il ne faut pas mettre des farces trop liquides (pas de fruits frais ou surgelés).
- On peut le faire aussi bien avec de la levure fraîche, qu'avec de la levure déshydratée (rapport 1:2 ou 1:2.5).
- On peut mettre que la moitié du sucre au début, puis incorporer le reste avec le gras à la fin.
- On peut mettre que 3 œufs au début, puis incorporer le 4e tout à la fin (après le gras).
- Je fais 2 pâtes (2 × 750 g de farine). Avec ça je fais 3 brioche tressé (520 g), une grande (1000 g) et une petite (570 g).
- On peut le faire avec moins levure fraîche (25 g), si la pâte repose toute une nuit au frigo.
- Pour les brioches en forme, on peut soit séparer en trois et garnir avec de la confiture et fruit secs entre les « étages », soit concassé de la nougatine et mélanger à la pâte, comme pour une « praluline » (mais dans ce cas pas de la confiture) et faire des pliages.
- Pour la brioche tresses, on sépare la pâte en trois (3 × 175 g), on fait des rectangles très allongés. On remplie deux des rectangles avec de la farce, le troisième juste avec du sucre. On ferme pour faire des grosse mèches, puis on les tresse.
