# Meringues

## Ingrédients

- blancs d'œufs : 140 g (≈4 œufs)
- sucre en poudre : 280 g *(le double des blancs)*
- sel : 1 pincée
- jus de citron : 5 ml (≈1 c.c.)
- Maïzena : 4 g (≈1 c.c.)

## Temps

- préparation : 30 min
- cuisson : 1h30 -- 2 h

## Préparation

Battre les blancs en neige très ferme avec le sel. Ajouter le sucre peu à peu tout en battant. Ajouter le citron, battre, puis la Maïzena, battre encore. Le mélange doit être très ferme et brillant.

## Cuisson

À plus de 100 °C la meringue cuit (devient jaunâtre), à moins de 100 °C elle sèche. Il faut ouvrir le four toutes les 20 -- 30 min pour faire sortir l'humidité.

- 1h15 à 120 °C
- 1h30 à 100 °C
- 2 h à 90 °C

## Note

On peut remplacer le citron par du vinaigre blanc (d'alcool ou de vin blanc).
