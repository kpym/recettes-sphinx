# Pain - Ciabatta

## Ingrédients
- farine : 500 g
- eau tiède : 380 ml
- levure sèche boulangère : 1,5 sachets ≈ 7 g
- sucre : 5-10 g
- sel : 10-20 g

## Temps
- préparation : 15 min + 15 min
- cuisson : 25 min

## Préparation

On met la levure dans l'eau, puis la moitié de la farine et le sucre. On mélange bien. On rajoute le reste de la farine et le sel. On mélange encore.

On sépara la pâte dans 3-4 bols huilés à huile d'olive. On couvre de film plastique. On laisse reposer ≈ 2 heures à 40°C.

On préchauffe à max (≈ 230-250°C).

On forme les ciabattas à l'aide d'un tapis en silicone bien fariné la touchant le moins possible et en essayant de garder les poches d'air à l'intérieur.

On fait cuire 10 min à max, puis on baisse à 200°C et on cuit encore 15 min.

## Note

- La farine doit avoir au moins 12% de protides.
- La source principale est [cette vidéo de cuoredicioccolato](https://youtu.be/3uW5zJcwGKg).
