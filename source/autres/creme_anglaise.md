# Crème anglaise

## Ingrédients

- jaunes d'œufs : 3
- lait : 500 ml
- sucre glace : 80 g
- maïzena : 1 c.-à-c. (5 g)
- vanille

## Préparation

On fait bouillir le lait. Pendant ce temps on mélange les autres ingrédients dans une casserole, on fouette. On verse le lait petit à petit, toute en fouettant. On fait épaissir à feux doux _(la crème ne doit pas bouillir !)_ toute en touillant en permanence avec le fouet. La cuisson est terminée lorsque la mousse à complètement disparu (la crème nappe alors la spatule). Fouettez vivement la crème hors du feu afin qu'elle devienne bien lisse. Refroidir au frigo quelques heures.

## Note

- Si par malheur la crème a bouilli elle forme des petits grumeaux, on peut la sauver quand même : une fois refroidie, on la bat vigoureusement au batteur électrique. La maïzena réduit le risque de grumeaux.
- On peut mettre un œuf de plus et un peu moins de sucre.
- À la sortie du feu on peut passer la crème dans une passoire.
- On peut la faire au micro-ondes : 4–5 min à puissance maximale pour chauffer le lait, puis 2–3 min pour épaissir, tout en remuant après chaque minute.
