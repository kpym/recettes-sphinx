# Les mesures (cuillères et tasses)

## 1 cuillère à café (pleine)

- 5 ml de liquide
- 5 g d'huile
- 6 g de sucre en poudre
- 6 g de sel
- 4 g de farine
- 4 g de cacao en poudre

## 1 cuillère à soupe (pleine)

- 15 g de sucre en poudre
- 15 g de sel
- 12 g d'huile
- 12 g de farine
- 12 g de cacao en poudre

## 1 cuillère à soupe (raze)

- 10 ml de liquide

## 1 tasse à café

- allemande : 125 ml
- française : 150 ml

## 1 tasse

- anglaise : 284 ml
- métrique : 250 ml
- américain : 240 ml _(prise pour référence)_

- riz : 200 g
- farine : 125 g
- sucre en poudre : 200 g
- sucre glace : 115 g
- huile : 215 g


