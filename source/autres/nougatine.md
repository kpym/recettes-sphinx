# Nougatine

## Ingrédients

- sucre : 250 g
- amandes effilées : 125 g

## Temps

- préparation : 20 min
- cuisson : 10 min

## Préparation

Faire dorer les amandes au four 5 min à 180°C. Pendant ce temps faire chauffer le sucre dans une casserole (4-5 min à feu moyen pour obtenir du caramel pas trop foncé). Arrêter le feu et verser les amandes grillées dans la casserole. Mélanger. Verser sur du papier sulfurisé. Écraser en fin galette antre deux feuilles de papier sulfurisé avec le rouleau à pâtisser. Enlever le papier sulfurisé quand la nougatine est tiède.

## Cuisson

- À 180 °C : 5 min (les amandes au four)
- À deux moyen : 5 min (le sucre sur la plaque)

## Notes

- Quand le caramel commence à prendre on peut touiller (contrairement aux recommandations officielles).
- Il ne faut enlever le papier ni trop tôt (car ça fait des filaments) ni trop tard (car ça ne se décolle pas bien).
- On peut remplacer les amandes effilées par d'autres fruits secs concassés.
