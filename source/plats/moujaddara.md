# Moujaddara (lentilles et riz)
(entre une soupe et un ragoût)

## Ingrédients

- oignons : 2
- échalotes : 4
- lentilles vertes : 1 verre
- riz : 3/4 de verre
- huile d'olive
- gras de canard _(pas obligatoire)_
- ail : 2 gousses
- cumin moulu : 1 cuillère à café de
- piment de Cayenne
- des herbes de province
- bouillon de légumes : 1 cube _(pas obligatoire)_
- jus de citron : 1/2 citron
- sel, fleur de sel, poivre
- des herbes fraîches (au choix)

## Temps
- préparation : 20 min
- cuisson : 1 h

## Préparation
- Caraméliser légèrement à feu doux les oignons et les échalotes finement coupés avec un peu de sel. _(15 min)_
- Réserver la moitié des oignons/échalotes. Rajouter l'ail, le cumin, le piment aux restes des oignons/échalotes. Cuire quelques minutes en mélangeant bien.
- Rajouter 5 tasse d'eau (ou de bouillon), de l'huile d'olive et du sel. Rajouté les lentilles et faire cuire de sorte à ce que les lentilles soient presque cuites. _(20 min)_
- Rajouter le riz et continuer à cuire. Si nécessaire couvrir, rajouter de l'eau, ou au contraire, laisse s'évaporer. À la fin il faut que ce soit un peu liquide, mais pas une soupe. _(20 min)_
- Rajouter du poivre (pas mal) et du jus de citron.
- Servir dans des bols garnis des oignons/échalotes frits, de flocon de piment, des herbes fraîches ciselés et de fleur de sel.

## Notes
- Source : [NY Times](https://www.nytimes.com/2020/03/30/dining/pantry-mujadara-coronavirus.html)
- Si vous remplacer les lentilles vertes (temps de cuisson 20--25 min), il faut savoir que le temps de cuisson des lentilles corail est 10--15 min et des blondes est 30--35 min.
