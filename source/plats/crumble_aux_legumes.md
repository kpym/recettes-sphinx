# Crumble aux légumes

## Ingrédients

## Légumes et épices

- aubergine : 1
- poivrons : 1 rouge et 1 vert
- tomate : 1 kg
- courgettes : 3 (bien fermes)
- oignon : 1
- gousse d'ail : 1
- cuillères à soupe d'huile d'olive : 3
- Thym
- Romarin
- Poivre
- Sel

## Pour les miettes

- farine : 150 g
- beurre : 100 g
- pignons : 40 g
- parmesan : 60 g
- Sel

## Temps
- préparation : 45 min
- cuisson : 35 min + 25 min

## Préparation
Émincez l'oignon et hachez très finement l'ail après en avoir ôté le germe. Faites chauffer l'huile dans une cocotte et faites y revenir l'ail et l'oignon.

Coupez l'aubergine en petits cubes et mettez-les dans la cocotte. Lorsqu'elles sont bien dorées, ajoutez les poivrons coupés en lanières. Assaisonnez avec le thym, le romarin, le sel et le poivre.

Au bout de 20 min, ajoutez les courgettes coupées en rondelles et les tomates pelées et épépinées. Mélangez bien et maintenir une cuisson assez vive pour que les légumes rendent leur eau.

Pendant ce temps, préchauffez le four à 180 °C et confectionnez les miettes : mélangez du bout des doigts la farine, le parmesan et le beurre. Ajoutez les pignons.

Versez les légumes dans un plat et répartissez les miettes dessus. Laissez cuire dans le four chaud 20 à 30 min.

## Note
- source : [Marmitons](http://www.marmiton.org/recettes/recette_crumble-aux-legumes_26758.aspx)
- à la place d'une cocotte j'utilise le wok
