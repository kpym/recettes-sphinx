# Peposo de jarret de bœuf

## Ingrédients

- jarret de bœuf : 2 grandes tranches
- vin rouge corsé : 1 bouteille
- coulis (passata) de tomates : 1 bouteille
- grains de poivre concassés : 15
- sel : 3 pincées
- gousses d'ail : 4
- sauge : 1 branche
- thym (frais) : 1 branche
- romarin : 1 branche
- carottes : 10

## Temps
- préparation : 15 min
- cuisson : 4 h

## Préparation

Assaisonner la viande et frotter la moelle avec du sel pour éviter qu'elle ne s'échappe. Mettre tous les ingrédients dans la cocotte, la refermer, porter à ébullition, réduire le feu et cuire pendant 4 heures !

Vérifier la cuisson et la réduction de la sauce. Ajoutez du vin ou de l'eau au besoin.

## Notes

1. Un bon vin fait une bonne sauce !
1. N'hésitez pas à préparer à l'avance et à réchauffer : la sauce sera encore meilleure.
1. Polenta, pâtes, purée... faites votre choix pour l'accompagnement et n'oubliez pas le pain pour saucer.

## Source

[cathetoiles.fr/peposo-de-jarret-de-boeuf-vin-tomate-et-poivre](http://web.archive.org/web/20220525015419/https://cathetoiles.fr/peposo-de-jarret-de-boeuf-vin-tomate-et-poivre)
