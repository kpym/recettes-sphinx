# Poivrons farcis « Lelia Svetla »

## Ingrédients
- oignon : 1 grosse bulbe
- carottes : 2
- poivrons : 6-7
- riz : 1 verre
- tomates concassées : 1 conserve
- veau haché : 700 g
- sel
- poivre
- persil
- paprika
- huile d'olive
- farine : 1,5 cuillères
- œuf : 1
- yaourt : 1 cuillère

## Temps
- préparation : 40 min
- cuisson : 30 min

## Préparation

### la farce
On fait revenir dans une poêle l'oignon + les restes des poivrons décapuchés + le riz +la viande. On rajoute les tomates + sel + poivre + paprika + persil haché.

### les poivrons
On sale les poivrons de l'intérieur, on les remplit avec la farce. On les range dans la casserole et on les saupoudre de farine et on met de l'eau jusqu'au bord des poivrons. On pose une assiette par-dessus. On fait cuire 30 min.

### la sauce
Dans une casserole : la farine + l'œuf + le yaourt. On rajoute lentement la sauce de la cuisson en touillant. On chauffe, sans porter à ébullition.

