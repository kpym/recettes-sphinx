# Imam bayidli (« l'imam s'est évanoui »)

## Ingrédients

- aubergines : 5
- oignons : 4
- carotte : 1
- ail (gousses):5
- tomates : 4
- huile d'olive
- sel
- poivre

## Temps
- préparation : 40 min
- cuisson : 15 min à poêle + 35 min au four à 190 °C

## Préparation

Épluchez les aubergines en bandes, puis les couper en grosses rondelles. Les saler et les laisser 30 min à côté.

On fait revenir dans l'huile d'olive les oignons et les carottes finement hachés. Après quelques minutes, on rajoute les gousses d'ail coupées. Après 1-2 min, on rajoute les tomates coupées en dés et on laisse encore 2-3 minutes. On retire du feu.

Égouttez les aubergines et les faire revenir dans de l'huile d'olive (pas très longtemps). Puis dans un plat mettre, en quatre couches successives, la moitié des aubergines, la moitié des autres légumes, ... puis au four couvert d'alu (mais sans que ça touche la nourriture).
