# Poulet au lait de coco rapide

## Ingrédients
- cuisses de poulet : 4
- oignon : 1
- gousse d'ail : 1
- tomates : 2
- curry : 1 pincée *(plus selon désir)*
- quatre-épices : 1 pincée
- lait de coco : 20 cl
- poivre
- sel
- huile

## Temps
- préparation : 20 min
- cuisson : 1 h

## Préparation
- Assaisonnez les cuisses de poulet avec le sel, le poivre et les 4 épices.
- Dans une cocotte (ou autre), faites chauffer de l'huile et faites-y revenir le poulet. Laissez dorer.
- Ajoutez l'oignon, l'ail et les tomates. Laissez suer quelques minutes.
- Ajoutez de l'eau à hauteur. Ajoutez le curry.
- Après 10-15 min, versez le lait de coco. Laissez cuire jusqu'à épaississement de la sauce.
- Servez avec un bon riz basmati.

## Cuisson
À feu moyen : 1 h _(après les 10 min à feu vif pour dorer les cuisses)_

## Notes
Source : [Marmiton](https://www.marmiton.org/recettes/recette_poulet-au-lait-de-coco-rapide_36946.aspx)
