# Poulet aux écrevisses « Cathy »

## Ingrédients
- écrevisses : 12 _(grosses)_
- cuisses de poulet : 10
- échalotes : 2
- gousses ail : 2
- vin blanc : 2 verres
- farine : 1 cuillère à soupe
- concentré de tomates : 70 g
- crème fraîche : 20 cl
- piment de Cayenne
- sel
- poivre
- huile d'olive

## Temps
- préparation : 1h15
- cuisson : 55 min

## Préparation
- Faire revenir les écrevisses dans l'huile d'olive. Réserver. _(5 min)_
- Faire revenir les cuisses de poulet. Réserver. _(10 min)_
- Faire revenir 2 échalotes et 2 gousses d'ail. _(3 min)_
- Baisser le feu à 4,5 / 9. _La suite de la cuisson est à feu doux pour que ça n'accroche pas._
- Ajouter le poulet et mouiller avec un verre de vin blanc. _(10 min)_
- Mélanger le second verre de vin, la farine, le concentré de tomates et le piment de Cayenne. Puis mouiller avec. _(20 min)_
- Une fois le vin évaporé, ajouter les écrevisses, saler et poivrer, puis lier avec la crème fraîche. _(2 min)_
