# Dahl de lentilles corail

## Ingrédients
- lentilles corail : 300 g
- oignon jaune : 2
- gousses ail : 2
- gingembre frais : 2 cm
- tomates concassées : 400 g
- lait de coco : 20 cl
- bouillon de légumes : 50 cl
- cumin moulu : 1/2 cuillère à café
- coriandre moulue : 1/2 cuillère à café
- curcuma en poudre : 1/2 cuillère à café
- citron jaune : 1 _(le jus)_
- coriandre fraîche : quelques brins
- huile d’olive : 1 cuillère à soupe

## Temps
- préparation : 30 min
- cuisson : 30 min

## Préparation
- Rincez et égouttez les lentilles corail. Pelez les oignons et ciselez-les finement. Pelez et dégermez l’ail puis pressez-le avec le gingembre.
- Chauffez d’huile d’olive dans une grande sauteuse et faites-y revenir les oignons. Ajoutez les gousses d’ail, le gingembre pressé et les épices. Laissez cuire quelques instants.
- Ajoutez les lentilles, les tomates concassées, le lait de coco et le bouillon, mélangez bien puis laissez mijoter 20 minutes en remuant.
- À la fin de la cuisson, ajoutez le jus du citron et la coriandre fraîche. Mélangez bien et servez avec du riz basmati.

## Cuisson
Dans un wok : 30 min

## Notes
- La [recette d'origine](https://cuisine-addict.com/dahl-lentilles-corail/)
