# Soupe de poissons « Christèle »

## Ingrédients

### Pour la soupe

- poisson blanc (cabillaud) : 1/2 - 1 kg
- tomates : 500 g
- poireau : 1
- poivrons : 2
- gousses d'ail : 4
- safran (en poudre) : 1 dose (0.1 g)
- feuilles de laurier : 2
- thym séché
- d'huile d'olive
- sel, poivre

### Pour la « rouille »

- jaune d'œuf : 1
- moutarde : 1 cuillère à café
- gousses d'ail : 3
- huile d'olive
- paprika

### Pour servir

- tranches de pain grillé
- gousses d'ail

## Temps
- préparation : 1 h 30
- cuisson : 1 h

## Préparation

### La soupe

- Émincez les poireaux, les poivrons et hacher l'ail.
- Dans le wok, chauffer un fond d'huile d'olive. Faire revenir l'ail, puis rajouter les poireaux, puis rajouter les poivrons.
- Rajouter les épices (thym, laurier, safran, sel, poivre). Laisser mijoter 10 min.
- Couvrir d'eau et laisser mijoter 20 minutes.
- Rajouter les poissons (coupés en gros morceaux) en rajoutant de l'eau si nécessaire. Laisser cuire encore 20 minutes.

### La « rouille »

- Mélanger la moutarde et le jaune d'œuf.
- Battre et pendant ce temps faire couler l'huile d'olive en filet. En gros faire une mayonnaise.
- Rajouter l'ail pressé et le paprika.

### L'accompagnement

La soupe est à servir avec :
- la rouille,
- les tranches de pain grillets,
- les gousses d'ail.

On frotte les tranches de pain avec l'ail, puis on met de la rouille dessus, et on peut, ou pas, les tremper dans la soupe.

## Cuisson
- 1 h : 20 min revenir les légumes + 20 min mijoter avec de l'eau + 20 min cuire le poisson

## Notes
- [inspiration d'origine](http://www.elle.fr/Elle-a-Table/Recettes-de-cuisine/soupe-de-poissons-comme-a-Marseille-549534)

