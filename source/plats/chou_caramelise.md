# Chou caramélisé « Agnes »

## Ingrédients
- chou : 1/2
- huile : 50 ml
- sucre : 2-3 cuillères à soupe
- vinaigre : 1 cuillère à café
- carvi : 1 cuillère à café

## Temps
- préparation : 15 min
- cuisson : 1 h

## Préparation
On coupe le chou. On couvre le fond de la casserole avec le sucre et on le fait caraméliser. On rajoute l'huile, on touille, puis on rajoute le vinaigre, puis le chou. On fait revenir. On rajoute de l'eau (1 verre) et le carvi, on fait cuire (longtemps) à feu doux. On surveille, s'il n'y a pas assez d'eau, no en rajoute.

## Cuisson
À feu doux : 1 h _(après 10 min à feu vif)_

## Notes
- C'est une recette autrichienne _(Agnes [au] = Agnès [fr])_.
- Sur internet on trouve 15-25 min pour la cuisson du chou, mais il faut environ 1 h (ou plus) pour cette recette. Ou 20-30 min sous pression.
