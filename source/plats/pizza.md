# Pizza

## Ingrédients

### pour la pâte
_(pour des pâtons de ≈232 g → 28 cm)_

- farine : 00
- levure : sèche boulangère
- eau : tiède _(62 % de la farine)_

| Ingrédient | 4    | 5    | 6    | 8    |
| :----------| ---: | ---: | ---: | ---: |
| farine (g) | 560  | 700  | 840  | 1120 |
| eau (g)    | 347  | 434  | 520  | 694  |
| sel (g)    | 17   | 21   | 25   | 34   |
| levure (g) | 3.2  | 4    | 4.8  | 6.4  |
| total (g)  | 927  | 1159 | 1390 | 1855 |


## Temps
- préparation : 20+20 min
- repos : 24–72h

## Préparation

### Pétrir

#### Méthode 1

On verse l'eau et on dilue dedans la levure. On rajoute la moitié de la farine et on mélange bien. On attends 5-10 min. On rajoute le sel. On continue à mélanger en rajoutant presque toute la farine. Puis on ajuste le reste de la farine peu à peu pour obtenir la bonne consistance _(voir plus bas)_.

#### Méthode 2

On fait un puits dans la farine, on met le sel au bord de la jatte, la levure dans le puits, avec un peu d'eau. On commence à malaxer le milieu et plus la boule se forme, plus on rajoute peu à peu d'eau pour obtenir la bonne consistance _(voir plus bas)_.

### Finition de la pâte

La pâte est prête quand elle est bien élastique mais pas collante (ne reste pas aux doigts). S'il n'y a pas assez d'eau elle n'est pas assez molle et élastique (on en rajoute encore). S'il y a trop d'eau elle devient collante (on rajoute un peu de farine).
Il est préférable qu'elle soit tout légèrement collante _(taux de hydratation > 63 %)_ quand la cuisson sera dans un four à moins de 300 °C, car sinon elle s'assèche. Elle peut être même avec un taux de hydratation de 80 % si on la fait « à la Bonci ».

On sort la pâte de la jatte quand elle est encore collante. Puis on continue le travail sur le plan de travail en faisant des pliures et en incorporant peu à peu de la farine pour arriver à une pâte élastique, mais légèrement collante. On la laisse reposer 30 min dans la jatte à couvert. Puis on finit le travail sur le plan de travail en faisant des pliures et en ajoutant, si nécessaire, un peu de farine.
On laisse reposer la pâte 2–3 h à température ambiante dans la jatte à couvert. Puis on forme les pâtons qu'on dépose dans un plat huilé, ou dans des bols, couvert de film plastique. On laisse reposer au frigo si possible au moins 24 h. On travaille la pâte froide, car elle colle moins.

## Sauce tomate

On prépare la sauce en avance (comme la pâte) et on la conserve au frigo (comme la pâte).
On fait revenir de l'ail et des oignons (frais) dans de l'huile d'olive. On rajoute le colis de tomate avec un peu de concentré de tomates. On laisse mijoter avec de l'origan.

## Cuisson

Si on a une Ooni la cuisson est moins de 2 min par pizza. Sinon, on préchauffe le four à max (250 C° en général). Si on a une pierre on la laisse chauffer en bas du four pendant 30 min. On cuit le plus bas possible dans le four. Avec pierre 7 min, sans pierre 11 min.

## Notes

- La correspondance « diamètre de la pizza » → « poids de farine » est : 21 cm → 80 g, 28 cm → 140 g, 35 cm → 220 g, 42 cm → 315 g _(la formule étant `diamètre`² × 0.179 = `farine en grammes`)_. Avec 550 g de farine on fait 4 pizzas de 28 cm.
