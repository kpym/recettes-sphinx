Les recettes de Kpym
====================

.. toctree::
   :maxdepth: 2

   entrees
   plats
   desserts
   autres

-------

La version `PDF` est disponible à l'adresse https://readthedocs.org/projects/recettes/downloads/pdf/latest/.

La version `HTML` est disponible à l'adresse https://recettes.readthedocs.io.
